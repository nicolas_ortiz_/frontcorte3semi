import React from 'react';
import { FlatList, TextInput, ActivityIndicator, Image, ImageBackground, Text, View, StyleSheet } from 'react-native';

export default class HomeScreen extends React.Component {

  static navigationOptions = ({ navigation }) => {

    return {

      title: 'Home',
      headerTitleStyle: { color: 'white' },

      headerStyle: {
        backgroundColor: '#2C2E35',
        textAlign: 'center'
      },
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      correo: '',
      nombre: '',
      id: 0,
      urlLaravel: "https://facebook.github.io/react-native/movies.json",
      isLoading: true
    }
  }

  componentDidMount() {
    return fetch('https://facebook.github.io/react-native/movies.json')
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          isLoading: false,
          dataSource: responseJson.movies,
        }, function () {

        });

      })
      .catch((error) => {
        console.error(error);
      });
  }

  obtenerDatos = async () => {

    return fetch(this.state.urlLaravel)
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          isLoading: false,
          dataSource: responseJson.data,
        }, function () {

        });

      })
      .catch((error) => {
        console.error(error);
      });
  }

  FlatListItemSeparator = () => {
    return (
      //Item Separator
      <View style={{ height: 0.5, width: '100%', backgroundColor: '#FFFFFF' }} />
    );
  };


  GetUser = (item, item2, item3) => {

    this.setState({ nombre: item });

    this.setState({ correo: item2 });

    this.setState({ id: item3 });

  }

  render() {

    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      )
    }

    return (
      <ImageBackground source={require('./UnaImagenMas/un_imagen_mas.jpg')} style={styles.container}>

        <View style={styles.viewContainer}>
        <FlatList
          data={this.state.dataSource}
          ItemSeparatorComponent={this.FlatListItemSeparator}
          renderItem={({item}) => 
          <Text style={styles.item}>Pelicula: {item.title} - Año: {item.releaseYear}</Text>}
          keyExtractor={({id}, index) => id}
        />
        </View>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({

  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    width: "100%",
    height: "100%",
    flexWrap: 'wrap',
  },
  viewContainer: {
    width: '100%',
    height: '50%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {
    color: '#FFFFFF',
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  inputContainer: {
    backgroundColor: '#FFFFFF',
    borderRadius: 30,
    borderBottomWidth: 1,
    width: "80%",
    height: "7%",
    flexDirection: 'row',
    alignItems: 'center'
  },
  inputs: {
    height: "80%",
    marginLeft: 16,
    borderBottomColor: '#FFFFFF',
    flex: 1,
  },
  inputIcon: {
    width: "7%",
    height: "39%",
    marginLeft: 15,
    justifyContent: 'center'
  },
});



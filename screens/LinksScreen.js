import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { ExpoLinksView } from '@expo/samples';

export default function LinksScreen() {
  return (
    <ScrollView style={styles.container}>
      {/*<View style={[styles.inputContainer, { marginTop: 25 }]}>
  <Image style={styles.inputIcon} source={{ uri: 'https://img.icons8.com/ios/50/000000/user-group-man-man.png' }} />
  <TextInput style={styles.inputs}
    ref={(al) => { this.nombre = al; }}
    placeholder="Nombre"
    keyboardType="email-address"
    underlineColorAndroid='transparent'
    onChangeText={nombre => this.setState({ nombre })}
    value={this.state.nombre} />
</View>

<View style={styles.inputContainer}>
  <Image style={styles.inputIcon} source={{ uri: 'https://img.icons8.com/dotty/80/000000/important-mail.png' }} />
  <TextInput style={styles.inputs}
    ref={(el) => { this.correo = el; }}
    placeholder="Correo"
    keyboardType="email-address"
    underlineColorAndroid='transparent'
    onChangeText={correo => this.setState({ correo })}
    value={this.state.correo} />
</View>

<View style={styles.MainContainer} key={this.state.updateForcing}>

  <FlatList
    data={this.state.dataSource}
    ItemSeparatorComponent={this.FlatListItemSeparator}
    renderItem={({ item }) =>
      <Text style={styles.item}
        onPress={this.GetUser.bind(this, '' + item.name, '' + item.email, '' + item.id)}>{item.name} - {item.email}</Text>}
    keyExtractor={({ id }, index) => id}
  />
</View>*/}
      <ExpoLinksView />
    </ScrollView>
  );
}

LinksScreen.navigationOptions = {
  title: 'Links',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
